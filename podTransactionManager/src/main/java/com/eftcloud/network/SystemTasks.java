package com.eftcloud.network;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.sun.management.OperatingSystemMXBean;
import java.io.File;

/**
 * Contiene operaciones del sistema para obtener información del sistema,
 * uso de memoria, llamadas al garbage colletor y enumeración de procesos.
 * 
 * @author Sonny Benavides (sbenav1@bancodebogota.com.co)
 * @version 1.5, 2017/08/29
 * @since 1.0
 */
public class SystemTasks {
    private static final String DISK = "disk";
    private static final String OS_NAME = "os.name";
    private static final String OS_VERSION = "os.version";
    private static final String OS_PATH = "sun.os.patch.level";
    private static final String OS_ARCH = "os.arch";
    private static final String PROCESSOR_IDENTIFIER = "processor_identifier";
    private static final String NUMBER_OF_PROCESSORS = "number_of_processors";
    private static final String COMPUTER_NAME = "computername";
    private static final String USER_DOMAIN = "userdomain";
    private static final String USER_NAME = "user.name";
    private static final String JAVA_RUNTIME = "java.runtime.name";
    private static final String JAVA_VERSION = "java.version";
    
    private static final String SYSTEM_MEMORY = "systemMemory";
    private static final String JVM_TOTAL_MEMORY = "jvmTotalMemory";
    private static final String JVM_MAX_MEMORY = "jvmMaxMemory";
    private static final String JVM_FREE_MEMORY = "jvmFreeMemory";
    private static final String JVM_USED_MEMORY = "jvmUsedMemory";
    private static final String USED_HEAP_MEMORY = "usedHeapMemory";
    
    private static final String TIME_FORMAT = "yyyy/MM/dd HH:mm:ss";
    private static final long KILO_BYTE = 1024L;
    private static final long MEGA_BYTE = KILO_BYTE * KILO_BYTE;
    private static final String PREFIX_MEGA_BYTES = " MB";
    private static final String NOT_AVAILABLE = "N.A.";
    private static final long SLEEP_INTERVAL = 100;
    private static final long SLEEP_INTERVAL_GT = 10;

    public static String obtainCurTime() {
        DateFormat formatter = new SimpleDateFormat(TIME_FORMAT);
        return formatter.format(new Date());
    }
    
    public static Map<String, Object> displaySystemInfo() {
        Map<String, Object> map = new HashMap();
        
        String disk = NOT_AVAILABLE;
        try {
            long diskSize = new File("/").getTotalSpace() / MEGA_BYTE;
            disk = diskSize + PREFIX_MEGA_BYTES;
        } catch(Exception ex) {}
        
        map.put(DISK, disk);
        map.put(OS_NAME, obtainProperty(OS_NAME));
        map.put(OS_VERSION, obtainProperty(OS_VERSION));
        map.put(OS_PATH, obtainProperty(OS_PATH));
        map.put(OS_ARCH, obtainProperty(OS_ARCH));
        
        map.put(PROCESSOR_IDENTIFIER, obtainEnv(PROCESSOR_IDENTIFIER));
        map.put(NUMBER_OF_PROCESSORS, obtainEnv(NUMBER_OF_PROCESSORS));
        map.put(COMPUTER_NAME, obtainEnv(COMPUTER_NAME));
        map.put(USER_DOMAIN, obtainEnv(USER_DOMAIN));
        
        map.put(USER_NAME, obtainProperty(USER_NAME));
        map.put(JAVA_RUNTIME, obtainProperty(JAVA_RUNTIME));
        map.put(JAVA_VERSION, obtainProperty(JAVA_VERSION));
        return map;
    }

    private static String obtainEnv(String key) {
        String value = System.getenv(key);
        if (value == null) {
            value = NOT_AVAILABLE;
        }
        return value;
    }

    private static String obtainProperty(String key) {
        String value = System.getProperty(key);
        if (value == null) {
            value = NOT_AVAILABLE;
        }
        return value;
    }
    
    public static Map<String, Object> displayMemory() {
        Runtime runtime = Runtime.getRuntime();
        long memorySize = ((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalPhysicalMemorySize() / MEGA_BYTE;
        long totalMemoryMB = runtime.totalMemory() / MEGA_BYTE;
        long maxMemoryMB = runtime.maxMemory() / MEGA_BYTE;
        long freeMemoryMB = runtime.freeMemory() / MEGA_BYTE;
        long usedMemoryMB = calcUsedMemory() / MEGA_BYTE;
        long usedHeapMemoryMB = calcUsedHeapMemory()/ MEGA_BYTE;
        
        Map<String, Object> map = new HashMap();
        map.put(SYSTEM_MEMORY, memorySize + PREFIX_MEGA_BYTES);
        map.put(JVM_TOTAL_MEMORY, totalMemoryMB + PREFIX_MEGA_BYTES);
        map.put(JVM_MAX_MEMORY, maxMemoryMB + PREFIX_MEGA_BYTES);
        map.put(JVM_FREE_MEMORY, freeMemoryMB + PREFIX_MEGA_BYTES);
        map.put(JVM_USED_MEMORY, usedMemoryMB + PREFIX_MEGA_BYTES);
        map.put(USED_HEAP_MEMORY, usedHeapMemoryMB + PREFIX_MEGA_BYTES);
        return map;
    }
    
    public static long calcUsedMemory() {
        Runtime runtime = Runtime.getRuntime();
        callGarbageCollectorGT();
        long memoryInBytes = runtime.totalMemory() - runtime.freeMemory();
        return memoryInBytes;
    }
    
    public static long calcUsedHeapMemory() {
        callGarbageCollectorGT();
        MemoryMXBean memBean = ManagementFactory.getMemoryMXBean();
        MemoryUsage heap = memBean.getHeapMemoryUsage();
        MemoryUsage nonheap = memBean.getNonHeapMemoryUsage();
        long memoryInBytes = heap.getUsed() + nonheap.getUsed();// + 6800000L;
        return memoryInBytes;
    }

    public static void callGarbageCollector() {
        try {
            System.gc();
            Thread.sleep(SLEEP_INTERVAL);
            System.runFinalization();
            Thread.sleep(SLEEP_INTERVAL);
            System.gc();
            Thread.sleep(SLEEP_INTERVAL);
        } catch (InterruptedException ex) {}
    }

    public static void callGarbageCollectorGT() {
        try {
            System.gc();
            Thread.sleep(SLEEP_INTERVAL_GT);
            System.runFinalization();
            Thread.sleep(SLEEP_INTERVAL_GT);
            System.gc();
            Thread.sleep(SLEEP_INTERVAL_GT);
        } catch (InterruptedException ex) {}
    }
    
    public static String displayProcesses() {
        StringBuilder builder = new StringBuilder();
        ThreadGroup threadGroup = Thread.currentThread().getThreadGroup();
        ThreadGroup parent;
        while((parent = threadGroup.getParent()) != null) {
            if(threadGroup != null) {
                threadGroup = parent;
                if(null != threadGroup) {
                    Thread [] threadList = new Thread[threadGroup.activeCount()];
                    threadGroup.enumerate(threadList);
                    for (Thread thread : threadList) {
                        builder.append(thread.getThreadGroup().getName())
                        .append("$").append(thread.getName()).append(" [Prior:")
                        .append(thread.getPriority()).append("] [Id:").append(thread.getId()).append("]");
                    }
                }
            }
        }
        return builder.toString();
    }
}
