package com.eftcloud.network;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Clase para recibir paquetes TCP de forma asíncrona con una cola concurrente.
 * 
 * @author Sonny Benavides (sbenav1@bancodebogota.com.co)
 * @version 1.5, 2017/08/29
 * @since 1.0
 */
public class TCPReceiver implements Runnable {
    private static final int QUEUE_MAX_CAPACITY = 10000;
    public final ConcurrentLinkedQueue<byte[]> inbox = new ConcurrentLinkedQueue<byte[]>();
    private final ExecutorService service = Executors.newSingleThreadExecutor();
    private TCPLink tcpLink = null;
    private int receivingErrors = 0;
    private int queueErrors = 0;
    
    public TCPReceiver(TCPLink tcpLink) {
        this.tcpLink = tcpLink;
    }
    
    public Queue<byte[]> getInbox() {
        return inbox;
    }
    
    public void remove(byte[] packet) {
        inbox.remove(packet);
    }
    
    @Override
    public void run() {
        while (tcpLink.isConnected()) {
            try {
                byte[] packet = tcpLink.receive();
               // System.out.println("TCPReceiver:run"+ Util.print4BytesStr(packet) + " --inbox:" + String.valueOf(inbox.size()));
                if(packet != null) {
                    inbox.add(packet);
                }
            
            // Timeout se presentará cada vez que el hilo intente leer el buffer.
            } catch (SocketTimeoutException stex) {
                //Logger.debug("TCPReceiver:run:stex", stex.toString());

            // Excepciones:
            // java.net.SocketException: Software caused connection abort: recv failed
            // java.net.SocketException: Socket is closed
            } catch (IOException ioex) {
                System.out.println("TCPReceiver:run:ioex" + ioex.toString());
                receivingErrors++;
            }
        }
    }

    public void init() {
        service.execute(this);
    }
    
    public void dispose() {
	service.shutdown();
        inbox.clear();
    }
    
    public void purge() {
        receivingErrors = 0;
        queueErrors = 0;
        if (inbox.size() > QUEUE_MAX_CAPACITY) {
            inbox.clear();
        }
    }

    public int getReceivingErrors() {
        return receivingErrors;
    }

    public int getQueueErrors() {
        return queueErrors;
    }
}
