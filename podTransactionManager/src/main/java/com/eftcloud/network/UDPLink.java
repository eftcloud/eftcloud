package com.eftcloud.network;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Clase para enviar y recibir paquetes UDP.
 * En Java 7 es posible obtener el loopbakc con InetAddress.getLoopbackAddress()
 * 
 * @author Sonny Benavides (sbenav1@bancodebogota.com.co)
 * @version 1.2, 2016/07/28
 * @since 1.0
 */
public class UDPLink implements Link {

    /**
     * Constante que define en 4096 bytes el tamaño del buffer por defecto.
     */
    private static final int DEFAULT_RECEIVED_BUFFER = 4096;
    
    /**
     * Constante que define en 5 segundos el tiempo de espera por defecto.
     */
    private static final int DEFAULT_WAIT_TIMEOUT = 5000;

    /**
     * Variable para el control del tamaño del buffer en la recepción de datos en
     * bytes.
     */
    public static int receivedBuffer = DEFAULT_RECEIVED_BUFFER;
    
    /**
     * Variable para el control del tiempo de espera en la recepción de datos en
     * segundos.
     */
    public static int timeout = DEFAULT_WAIT_TIMEOUT;

    /**
     * Envía datos via UDP localmente, ajustando el puerto de destino.
     * @param targetPort Puerto de destino.
     * @param dataToSend Datos a enviar.
     * @return Respuesta del socket UDP.
     * @throws IOException enviando o recibiendo un datagrama.
     */
    public static String send(int targetPort, String dataToSend) throws IOException {
        return send(LOOPBACK.IP_V4, targetPort, dataToSend);
    }

    /**
     * Envía datos via UDP localmente, ajustando el puerto de destino.
     * @param targetPort Puerto de destino.
     * @param dataToSend Datos a enviar en un arreglo de bytes.
     * @return Respuesta del socket UDP.
     * @throws IOException enviando o recibiendo un datagrama.
     */
    public static byte[] send(int targetPort, byte[] dataToSend) throws IOException {
        return send(LOOPBACK.IP_V4, targetPort, dataToSend);
    }

    /**
     * Envía datos via UDP, ajustando la dirección IP y el puerto de destino.
     * @param targetAddress Dirección IP de destino.
     * @param targetPort Puerto de destino.
     * @param dataToSend Datos a enviar.
     * @return Respuesta del socket UDP.
     * @throws IOException enviando o recibiendo un datagrama.
     */
    public static String send(String targetAddress, int targetPort, String dataToSend) throws IOException {
        InetAddress address = InetAddress.getByName(targetAddress);
        return send(address, targetPort, dataToSend);
    }

    /**
     * Envía datos via UDP, ajustando la dirección IP y el puerto de destino.
     * @param targetAddress Dirección IP de destino.
     * @param targetPort Puerto de destino.
     * @param dataToSend Datos a enviar en un arreglo de bytes.
     * @return Respuesta del socket UDP.
     * @throws IOException enviando o recibiendo un datagrama.
     */
    public static byte[] send(String targetAddress, int targetPort, byte[] dataToSend) throws IOException {
        InetAddress address = InetAddress.getByName(targetAddress);
        return send(address, targetPort, dataToSend);
    }

    /**
     * Envía datos via UDP escribiendo y leyendo a través de un DatagramSocket.
     * Lanza una excepción SocketTimeoutException cuando no hay respuesta desde
     * el servidor despu�s de 5 segundos.
     * @param targetAddress Dirección IP de destino.
     * @param targetPort Puerto de destino.
     * @param dataToSend Datos a enviar.
     * @return Respuesta del socket UDP.
     * @throws IOException enviando o recibiendo un datagrama.
     */
    public static String send(InetAddress targetAddress, int targetPort, String dataToSend) throws IOException {
        byte[] data = send(targetAddress, targetPort, dataToSend.getBytes());
        String received = new String(data);
        return received;
    }
    
    /**
     * Envía datos via UDP escribiendo y leyendo a través de un DatagramSocket.
     * Lanza una excepción SocketTimeoutException cuando no hay respuesta desde
     * el servidor después del tiempo definido en la variable timeout.
     * @param targetAddress Dirección IP de destino.
     * @param targetPort Puerto de destino.
     * @param dataToSend Datos a enviar en un arreglo de bytes.
     * @return Respuesta del socket UDP.
     * @throws IOException enviando o recibiendo un datagrama.
     */
    public static byte[] send(InetAddress targetAddress, int targetPort, byte[] dataToSend) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        dos.write(dataToSend);
        byte[] data = baos.toByteArray();
        DatagramPacket packet = new DatagramPacket(data, data.length, targetAddress, targetPort);
        DatagramSocket socket = new DatagramSocket();
        socket.send(packet);
        dos.close();
        packet = new DatagramPacket(new byte[receivedBuffer], receivedBuffer);
        socket.setSoTimeout(timeout);
        socket.receive(packet);
        return packet.getData();
    }
}