package com.eftcloud.network;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Interfaz para cualquier tipo de implementación que requiera funciones de
 * conectividad via UDP o TCP.
 *
 * @author Sonny Benavides (sbenav1@bancodebogota.com.co)
 * @version 1.1, 2016/05/05
 * @since 1.0
 */
public interface Link {

    /**
     * Recupera la dirección del loopback para el protocolo IP versión 4 y 6.
     */
    public static class LOOPBACK {
        static InetAddress IP_V4;
        static InetAddress IP_V6;
        static {
            try {
                IP_V4 = InetAddress.getByAddress(new byte[]{0x7F, 0x00, 0x00, 0x01});
                IP_V6 = InetAddress.getByAddress(
                    new byte[] {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01});
            } catch (UnknownHostException ex) {}
        }
    }
}