package com.eftcloud.network;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Controla una tarea en un tiempo determinado, emulando un timeout.
 * 
 * @author Sonny Benavides (sbenav1@bancodebogota.com.co)
 * @version 1.5, 2017/08/29
 * @since 1.0
 */
public class TimeoutTask extends TimerTask {

    private final Timer timer = new Timer();
    private boolean timeout = false;

    public void run() {
        timeout = true;
    }
    
    public void start(long duration) {
        timer.schedule(this, duration);
    }
    
    public void stop() {
        this.cancel();
        timer.cancel();
    }
    
    public boolean isTimeout() {
        return timeout;
    }
}
