package com.eftcloud.network;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;

/**
 * Contiene las funciones para recuperar la información de las tarjetas de red.
 * 
 * @author Sonny Benavides (sbenav1@bancodebogota.com.co)
 * @version 1.5, 2017/08/29
 * @since 1.0
 */
public class Networking {
    
    private static final String LOCAL_HOST = "localhost";
    private static final String LOOP_BACK = "127.0.0.1";
    private static final String LOOP_BACK_V6 = "0:0:0:0:0:0:0:1%1";
    private static final String SLASH = "/";
    public static String host = LOCAL_HOST;
    
    public static String resolveMachineIP() {
        System.setProperty("java.net.preferIPv4Stack" , "true");
        StringBuilder builder = new StringBuilder();
        try {
            String address = InetAddress.getLocalHost().getHostAddress();
            if (!address.equals(LOOP_BACK)) {
                host = address;
            }

            /*
             * Above method often returns "127.0.0.1", In this case we need to
             * check all the available network interfaces
             */
            Enumeration<NetworkInterface> nInterfaces = NetworkInterface.getNetworkInterfaces();
            while (nInterfaces.hasMoreElements()) {
                NetworkInterface netint = nInterfaces.nextElement();
                if (netint.isVirtual() || netint.isLoopback()) {
                    continue;
                }

                Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
                while (inetAddresses.hasMoreElements()) {
                    InetAddress iAddress = inetAddresses.nextElement();
                    if (iAddress instanceof Inet4Address) {
                        address = iAddress.getHostAddress();
                        String name = iAddress.getHostName();

                        if (!address.equals(name)) {
                            builder.append(name);
                            builder.append(SLASH);
                            builder.append(address);
                            builder.append(SLASH);
                        } else {
                            builder.append(name);
                            builder.append(SLASH);
                        }
                    }
                }
            }
        } catch (Exception ex) {}
        
        if (builder.length() > 0) {
            host = builder.toString();
        }
        return host;
    }

    /**
     * Obtiene la información de las tarjetas de red en formato JSON.
     * 
     * {"nets":
     * [
     *  {"displayName":"xxxx", "name":"yyyy", "up":"s", "loopback":"0000"
     *      "pointToPoint":"zzzz", "multicast":"s", "virtual":"s", "hw":"00:00:00",
     *      "mtu":"aaaa", "address":["10.84.64.31","10.84.64.32"]
     *  }
     * ]
     * }
     * @param filtered
     * @return La información de las tarjetas de red
     * @throws SocketException 
     */
    public static JSONArray displayInterfaces(boolean filtered) throws SocketException {
        List<Map> netintList = new ArrayList();
        Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
        for (NetworkInterface netint : Collections.list(nets)) {
            boolean isUp = netint.isUp();
            boolean isLoopback = netint.isLoopback();
            if (filtered && (!isUp || isLoopback)) {
                continue;
            }
            Map<String, Object> map = new HashMap();
            map.put("displayName", netint.getDisplayName());
            map.put("name", netint.getName());
            map.put("up", isUp);
            map.put("loopback", isLoopback);
            map.put("pointToPoint", netint.isPointToPoint());
            map.put("multicast", netint.supportsMulticast());
            map.put("virtual", netint.isVirtual());
            map.put("hw", Arrays.toString(netint.getHardwareAddress()));
            map.put("mtu", netint.getMTU());
            
            List<Map> addressList = new ArrayList();
            Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
            for (InetAddress inetAddress : Collections.list(inetAddresses)) {
                Map<String, Object> addressMap = new HashMap();
                addressMap.put("addr", inetAddress.getHostAddress());
                addressMap.put("name", inetAddress.getHostName());
                addressMap.put("local", inetAddress.isAnyLocalAddress());
                addressMap.put("link", inetAddress.isLinkLocalAddress());
                addressMap.put("mcGlobal", inetAddress.isMCGlobal());
                addressMap.put("mcLocal", inetAddress.isMCLinkLocal());
                addressMap.put("mcNode", inetAddress.isMCNodeLocal());
                addressMap.put("mcOrg", inetAddress.isMCOrgLocal());
                addressMap.put("mcSite", inetAddress.isMCSiteLocal());
                addressMap.put("multicast", inetAddress.isMulticastAddress());
                addressMap.put("siteLocal", inetAddress.isSiteLocalAddress());
                //addressList.add(inetAddress.toString());
                addressList.add(addressMap);

            }
            map.put("address", addressList);
            netintList.add(map);
        }
        JSONArray netintArray = new JSONArray(netintList);
        return netintArray;
    }
    
    public static String displayDomain() throws UnknownHostException {
        byte[] ipAddress = new byte[] { 127, 0, 0, 1 };
        InetAddress address = InetAddress.getByAddress(ipAddress);
        String hostnameCanonical = address.getCanonicalHostName();
        return hostnameCanonical;
    }
}
