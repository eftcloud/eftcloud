package com.eftcloud.network;

import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Clase para enviar paquetes TCP de forma asíncrona con una cola de acceso restringido.
 * 
 * @author Sonny Benavides (sbenav1@bancodebogota.com.co)
 * @version 1.5, 2017/08/29
 * @since 1.0
 */
public class TCPSender implements Runnable {
    private static final int QUEUE_MAX_CAPACITY = 10000;
    private final LinkedBlockingQueue<byte[]> outbox = new LinkedBlockingQueue();
    private final ExecutorService service = Executors.newSingleThreadExecutor();
    private TCPLink tcpLink = null;
    private int sendingErrors = 0;
    private int queueErrors = 0;
    
    public TCPSender(TCPLink tcpLink) {
        this.tcpLink = tcpLink;
    }
    
    public Queue<byte[]> getOutbox() {
        return outbox;
    }
    
    public void send(byte[] req) {
        try {
            outbox.put(req);                    // Pone el elemento al final de la cola con espera.
        } catch (InterruptedException ex) {
            queueErrors++;
        }
    }

    @Override
    public void run() {
        while (tcpLink.isConnected()) {
            try {
                byte[] packet = outbox.take();        // Recupera y elimina la cabeza de la cola con espera (FIFO).
                //Logger.debug("TCPSender:run", Util.print4BytesStr(packet) + " --outbox:" + String.valueOf(outbox.size()));
                tcpLink.send(packet);
            }   catch (InterruptedException iex) {
                queueErrors++;
                //Logger.debug("TCPSender:run:iex", iex.toString());
                
            // java.net.SocketException: Broken pipe
            // java.net.SocketException: Software caused connection abort: socket write error
            } catch (IOException ioex) {
                sendingErrors++;
               // Logger.debug("TCPSender:run:ioex", ioex.toString());
            }
        }
    }

    public void init() {
        service.execute(this);
    }
    
    public void dispose() {
	service.shutdown();
        outbox.clear();
    }

    public void purge() {
        sendingErrors = 0;
        queueErrors = 0;
        if (outbox.size() > QUEUE_MAX_CAPACITY) {
            outbox.clear();
        }
    }

    public int getSendingErrors() {
        return sendingErrors;
    }

    public int getQueueErrors() {
        return queueErrors;
    }
}
