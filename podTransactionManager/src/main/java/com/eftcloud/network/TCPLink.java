package com.eftcloud.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.util.Queue;
import java.util.Random;

/**
 * Clase para enviar y recibir paquetes TCP.
 * Trabaja en los siguientes modos:
 * Síncrono: con los métodos send y receive
 * Asíncrono: con los métodos sendT y receiveT
 * 
 * @author Sonny Benavides (sbenav1@bancodebogota.com.co)
 * @version 1.5, 2017/08/29
 * @since 1.0
 */
public class TCPLink implements Link {
    protected static final int RECEIVE_BUFFER = 8192;
    protected static final int RECEIVE_TIMEOUT = 20000;
    protected static final int RESTART_DELAY_TIME = 100;
    private static Socket socket = null;
    private static DataOutputStream dos;
    private static DataInputStream dis;
    private static boolean connected = false;

    public static int buffer = RECEIVE_BUFFER;
    public static int timeout = RECEIVE_TIMEOUT;

    private static volatile TCPLink instance = null;
    private TCPSender sender = null;
    private TCPReceiver receiver = null;

    protected TCPLink() {
        System.out.println("TCPLink:constructor"+ String.valueOf(new Random().nextInt()));
    }

    /**
     * Singleton del tipo thread-safety JVM 1.5.
     * @return 
     */
    public static TCPLink getInstance(){
        if(instance == null){
            synchronized (TCPLink.class) {
                if(instance == null){
                    instance = new TCPLink();
                }
            }
        }
        return instance;
    }

    public void open(String host, int port) throws IOException {
        //Logger.debug("TCPLink:open:In", host + ":" + String.valueOf(port) + " Sck:" + socket);
        if (socket == null || !socket.isConnected()) {
            socket = new Socket(host, port);
            socket.setReceiveBufferSize(buffer);
            socket.setSoTimeout(timeout);
            dos = new DataOutputStream(socket.getOutputStream());
            connected = true;
        }
        //Logger.debug("TCPLink:open:Out", "Sck:" + socket + " dis:" + dis + " dos:" + dos.size());
    }
    
    public void close() throws IOException {
        try {
            if (dos != null) {
                dos.close();
                dos = null;
            }
            socket.shutdownOutput();
        } catch (IOException ex) {}

        try {
            if (dis != null) {
                dis.close();
                dis = null;
            }
            socket.shutdownInput();
        } catch (IOException ex) {}

        if (socket != null) {
            socket.close();
            socket = null;
        }
        if (sender != null) {
            sender.purge();
        }
        if (receiver != null) {
            receiver.purge();
        }
        SystemTasks.callGarbageCollectorGT();
        connected = false;
    }

    public void forceRestart(String host, int port) throws IOException {
        try {
            close();
            Thread.sleep(RESTART_DELAY_TIME);
        } catch(Exception ex) {}

        open(host, port);
        try {
            Thread.sleep(RESTART_DELAY_TIME);
        } catch (InterruptedException ex) {}
    }
    
    /**
     * Envía datos via TCP.
     * @param packet
     * @throws IOException enviando un paquete.
     */
    public void send(byte[] packet) throws IOException  {
        
        if (dos != null) {
            short size = (short) packet.length;
            dos.writeShort(size);    // Comentado únicamente para Director. Mina se encarga de agregar el tamaño a la trama.
            dos.write(packet);
            dos.flush();
        }

        // Esta pausa evita colisión entre mensajes.
        try {
            Thread.sleep(5);
        } catch (InterruptedException ex) {}
    }
    
    public synchronized byte[] receive() throws IOException {
        byte[] packet = null;
        DataInputStream dis = new DataInputStream(socket.getInputStream());
        synchronized(dis) {
            try {
                int size = dis.readUnsignedShort();
                size = (short) (size & 0xFFFF);   // short: 16-bit signed two's complement integer (2 bytes)
                //Logger.debug("TCPLink:receive:sizeC", String.valueOf(size));
                packet = new byte[size];
                dis.readFully(packet);
                //Logger.debug("TCPLink:receive:packet", new String(packet));
            } catch (EOFException eof) {
                connected = false;
            }
        }
        return packet;
    }
    
    public boolean isConnected () {
        return socket != null && connected;
        //boolean host = socket.getInetAddress().isReachable(RECEIVE_TIMEOUT);
        //System.err.println("reacheable:" + host);

    }

    public void initTiming() {
        receiver = new TCPReceiver(instance);
        sender = new TCPSender(instance);
        receiver.init();
        sender.init();
    }

    public void disposeTiming() {
        receiver.dispose();
        sender.dispose();
    }
    
    /**
     * Requiere del proceso de Timming.
     * @param packet
     */
    public void sendT(byte[] packet) {
        sender.send(packet);
    }

    public Queue<byte[]> sendT() {
        return sender.getOutbox();
    }

    /**
     * Requiere del proceso de Timming.
     * @return 
     */
    public Queue<byte[]> receiveT()  {
        return receiver.getInbox();
    }

    /**
     * Requiere del proceso de Timming.
     * @param packet
     */
    public void receiveRemove(byte[] packet)  {
        receiver.remove(packet);
    }
    

    /**
     * Total de errores al enviar y recibir.
     * Los errores son controlados por la clase que llama a través de un
     * timeout.
     * Requiere del proceso de Timming.
     * @return 
     */
    public int countErrors() {
        int errors = sender.getSendingErrors() + receiver.getReceivingErrors();
        sender.purge();
        receiver.purge();
        return errors;
    }
}