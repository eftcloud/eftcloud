package com.eftcloud.transactor;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.Session;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.eftcloud.processor.Config;
import com.eftcloud.services.SendQueue;
@Component
public class ServerServices  implements Runnable  {

	public static int port=0;
	public static String ip_add;
	private static ServerSocket server;
	public static Socket socket_client ;
	private int id_server;
	private static Config config = new Config();
	private JSONObject setup_conn = new JSONObject();


	 @Autowired
	    static JmsTemplate jmsTemplate;

	    @Autowired
	    static Queue queue;
	    
	    static OutputStream os;
	
	    public ServerServices()
	    {
	    	
	    }
	    
	public ServerServices(int port_number, String address_ip,int queue_number, JSONObject js)
	{
		this.port=port_number;
		this.ip_add=address_ip;
		this.queue = config.queue_IN(queue_number);
		this.jmsTemplate=config.jmsTemplateManual();
		this.id_server=queue_number;
		this.setup_conn=js;


	}
	
	public void beginServices()
	{
		
		try {
			server= new ServerSocket(port, 1, InetAddress.getByName(ip_add));
			System.out.println("Server is listening on port " + port);
			String data = null;
			
            while (true) {
            	Socket socket_client = server.accept();
            	socket_client.setKeepAlive(true);
                System.out.println("New client connected");
                                    
                String clientAddress = socket_client.getInetAddress().getHostAddress();
                System.out.println("\r\nNew connection from " + clientAddress + " Port "+ socket_client.getPort() );
                
                ProcessorClient p= new ProcessorClient(socket_client,queue,jmsTemplate,this.id_server,setup_conn);
                Thread thread1 = new Thread(p, "Client "+socket_client.getPort());
                thread1.start();
                
                /*socket_client.setKeepAlive(true);
                DataInputStream dis = new DataInputStream(socket_client.getInputStream());
                DataOutputStream dos = new DataOutputStream(socket_client.getOutputStream());
                
                byte[] buffer = null;
                short sSize = dis.readShort();
                buffer = new byte[sSize];
                dis.readFully(buffer);
                System.out.println("Rcved: " + new String(buffer));
                data = new String(buffer);
                
               
               
                    putQueue(data);
                //}*/
            }
            
            
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void ProcessorInputMessage(Map<String,String> message) throws Exception {
		
		System.out.println("leyo " + message);
		
	}
	
	
	
	
	public static void stopServices()
	{
		try {
			server.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		beginServices();
	}

	
	
}
