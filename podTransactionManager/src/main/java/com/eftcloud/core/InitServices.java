package com.eftcloud.core;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.eftcloud.params.ParamDefinitions;
import com.eftcloud.processor.GetInformationServices;
import com.eftcloud.transactor.ServerServices;


/**
 * 
 * @author eftcloud
 * @version 1.0
 * @apiNote Inicializa las conexiones del Servicio 
 * 
 * ===============================================================================
 * 								Releases Notes
 * ===============================================================================
 * 
 * NAME								DATE					Description
 * -------------------------------------------------------------------------------
 * Carlos Mayo						18/08/2020				Version Inicial
 * 
 * 
 */

public class InitServices {

/**
 * Variables	
 */


public static Map <Integer, ServerServices> connServices =new HashMap<Integer, ServerServices>();

	
/**
 * @author eftcloud
 * @throws JSONException 
 * @apiNote Inicializa los servicios
 */
@SuppressWarnings("resource")
public static void initServers() throws JSONException
{
		/***
		 *  {
        "id_config": 1,
        "config_physical": "{}",
        "name_config": "Banco Dummy- Fontibon",
        "config_logical": "{'ip_config':'127.0.0.1','port_config':'9051','protocol':'TCP','type_service':'SERVER','type_message':'ISO8583'}",
        "id_owner_config": 1
    }
		 * */
	//Crear Servicios
	GetInformationServices.getDataConfig().forEach((key, value) -> {
		JSONObject item_value = new JSONObject();
		item_value=value;
		try {
			//Get Data
			int id_conn = Integer.parseInt(item_value.get(ParamDefinitions._CONST_VARIABLE_ID_CONFIG).toString());
			String name_conn=item_value.get(ParamDefinitions._CONST_VARIABLE_NAME_CONFIG).toString();
			String config_physical= item_value.get(ParamDefinitions._CONST_VARIABLE_PHYS_CONN).toString();
			JSONObject item_value_cfg = new JSONObject(config_physical);
			item_value_cfg.put(ParamDefinitions._CONST_VARIABLE_ID_CONFIG, id_conn);
			int port =Integer.parseInt(item_value_cfg.get(ParamDefinitions._CONST_VARIABLE_PORT_SERVER).toString());
			String ip_add=item_value_cfg.get(ParamDefinitions._CONST_VARIABLE_IP_ADDRESS).toString();
			
			if (item_value_cfg.get(ParamDefinitions._CONST_VARIABLE_TYPE_SERVICE).toString().equals(ParamDefinitions._CONST_TYPE_SERVICE_SERVER))
			{
				ServerServices s = new ServerServices(port,ip_add,id_conn,item_value_cfg);
				
				Thread thread1 = new Thread(s, "Service "+ name_conn+ " = " +id_conn);
		        thread1.start();
				connServices.put(key, s);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			//System.out.println("Key : " + key + " Value : " + value);
	});
	
	
	
}
	
	
}
