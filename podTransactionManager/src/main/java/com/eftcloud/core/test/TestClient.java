package com.eftcloud.core.test;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class TestClient {
	private Socket socket;
    private Scanner scanner;
    
    private TestClient()
    {
    	
    }
    
    private TestClient(InetAddress serverAddress, int serverPort) throws Exception {
        this.socket = new Socket(serverAddress, serverPort);
        this.scanner = new Scanner(System.in);
    }
    private void start() throws IOException {
        String input;
        while (true) {
            input = scanner.nextLine();
            PrintWriter out = new PrintWriter(this.socket.getOutputStream(), true);
            out.println(input);
            out.flush();
        }
    }
    
    public void litte_client(InetAddress serverAddress, int serverPort) throws IOException {
    	BufferedOutputStream outStream = null;
		BufferedReader receive_PackedResponseData = null;			
    	Socket connection = new Socket(serverAddress, serverPort);
    	String input;
    	 this.scanner = new Scanner(System.in);
    	while (true) {
    		input = scanner.nextLine();
			if (connection.isConnected()) {
				/*outStream = new BufferedOutputStream(
						connection.getOutputStream());
				outStream.w(input);
				outStream.flush();*/
				PrintWriter out = new PrintWriter(connection.getOutputStream(), true);
	            out.println(input);
	            out.flush();
	        
				
				System.out.println("-----Request sent to server---");
				System.out.println(input);
			}
	
			/** Receive Response */
	
			if (connection.isConnected()) {
				receive_PackedResponseData = new BufferedReader(
						new InputStreamReader(connection.getInputStream()));
				System.out.println("-- RESPONSE recieved---");
				System.out.println(receive_PackedResponseData.readLine());
				// System.out.println("Response : "+ISOUtil.hexString(receive_PackedResponseData.readLine().getBytes()));				
			}

    	}

    }
    
    public static void main(String[] args) throws Exception {
    	/*TestClient client = new TestClient(
                InetAddress.getByName(args[0]), 
                Integer.parseInt(args[1]));
        
        System.out.println("\r\nConnected to Server: " + client.socket.getInetAddress());
        client.start();   */
    	TestClient client = new TestClient();
    	client.litte_client(InetAddress.getByName(args[0]), Integer.parseInt(args[1]));
    }
}
