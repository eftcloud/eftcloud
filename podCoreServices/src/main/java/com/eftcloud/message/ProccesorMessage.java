package com.eftcloud.message;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.sql.Timestamp;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.packager.GenericPackager;
import org.jpos.iso.packager.ISO87APackager;
import org.jpos.iso.packager.ISO87APackagerBBitmap;
import org.jpos.iso.packager.ISO87BPackager;
import org.jpos.iso.packager.ISO93APackager;
import org.jpos.iso.packager.ISO93BPackager;
import org.json.JSONException;
import org.json.JSONObject;

import com.eftcloud.message.validator.ProccesorValidator;
import com.eftcloud.params.ParamDefinitions;





public class ProccesorMessage {

//Reflections Objects Definitions
ProccesorValidator proc_val = new ProccesorValidator();
	
public JSONObject getResponseMessage(JSONObject input_message) {
	
	JSONObject output_message= new JSONObject();
	
	try {
		//Parse for ISO8583
		String input_data=input_message.getString(ParamDefinitions._CONST_VARIABLE_MESSAGE);
		if (input_message.get(ParamDefinitions._CONST_VARIABLE_MSG_TYPE).equals(ParamDefinitions._CONST_TYPE_MSG_ISO8583))
		{
			String Msg=Proccess_ISO8583(input_data);
			output_message=input_message;
			output_message.put(ParamDefinitions._CONST_VARIABLE_RESPONSE, ParamDefinitions._CONST_RESPONSE_CODE_00);
			output_message.put(ParamDefinitions._CONST_VARIABLE_MESSAGE, Msg);
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			output_message.put(ParamDefinitions._CONST_VARIABLE_TIME_RESPONSE, timestamp);
		}
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	
	return output_message;
}

public String Proccess_ISO8583(String input_data) throws ISOException
{
	String parsed_message=input_data;
    String hexmsg = input_data;
    JSONObject mo = new JSONObject();
    try {
    // convert hex string to byte array
    byte[] bmsg =ISOUtil.hex2byte(hexmsg);
    ISOMsg m = new ISOMsg();
    InputStream is = getClass().getResourceAsStream("/fields.xml");
    GenericPackager packager = new GenericPackager(is);
    
    // set packager, change ISO87BPackager for the matching one.
    m.setPackager(packager);
    //unpack the message using the packager
    m.unpack(input_data.getBytes());
    //dump the message to standar output
    m.dump(System.out, "");
    //Try to Parser Message to Json
    mo=parseMessageToJson(m );
    System.out.println(mo);
    }catch (Exception e) 
    {
    	e.printStackTrace();
    }
	return parsed_message;
}

public JSONObject parseMessageToJson(ISOMsg isoMsg ) {
	JSONObject mo = new JSONObject();
	for(int i=0; i<=isoMsg.getMaxField(); i++){
        if(isoMsg.hasField(i))
        {
        	try {
				mo.put(i+"", isoMsg.getString(i));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	 System.out.println(i+"='"+isoMsg.getString(i)+"'");
        	 
        }
           
    }
	return mo;
}


public String ExecutorValidations(JSONObject message_in)
{
	try {
	JSONObject ex = new JSONObject();
	ex.put("function", "");
	Class<?> c = Class.forName("com.eftcloud.message.validator.ProccesorValidator");
    Object t = c.newInstance();
    Method[] allMethods = c.getDeclaredMethods();
    for (Method m : allMethods) {
	String mname = m.getName();
	
	m.setAccessible(true);
    Object o = m.invoke(t,ex, "00","05");
    System.out.println( mname + " => " + (String) o);

	
	 System.out.println(mname);
    }
   
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	return null;
}

public static void main(String arg[]) throws ISOException
{
	ProccesorMessage p = new ProccesorMessage();
	p.ExecutorValidations(null);
	//String input_data="080000200000008000001234563132333435363738";
	//System.out.println(p.Proccess_ISO8583("ISO0140000500800822000000000000004000000000000001002145307046398301"));
}


}
