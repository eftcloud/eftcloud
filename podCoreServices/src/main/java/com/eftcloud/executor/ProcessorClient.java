package com.eftcloud.executor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.Timestamp;

import javax.jms.Queue;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;

import com.eftcloud.params.ParamDefinitions;
import com.eftcloud.processor.ProccesorQueue;
import com.eftcloud.services.SendQueue;

public class ProcessorClient implements Runnable {
	
	 Socket socket_client;
	private ProccesorQueue proccesor_queue_in;
	private ProccesorQueue proccesor_queue_out;
	private int id_server;
	private JSONObject setup_conn = new JSONObject();
	 @Autowired
	    static JmsTemplate jmsTemplate;

	    @Autowired
	    static Queue queue;
	   
     public ProcessorClient(Socket socket,Queue q, JmsTemplate j,int server_id,JSONObject sc) {
        this.socket_client = socket;
        this.queue = q;
		this.jmsTemplate=j;
		this.id_server=server_id;
		this.setup_conn=sc;
     }

     public void putQueue(Object message) {
    	 JSONObject message_object =  new JSONObject();
         try {
        	 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				message_object.put(ParamDefinitions._CONST_VARIABLE_TIME_REQUEST, timestamp);
				message_object.put(ParamDefinitions._CONST_VARIABLE_MESSAGE, message);
				message_object.put(ParamDefinitions._CONST_VARIABLE_SOCKET_ID, socket_client.getPort());
				message_object.put(ParamDefinitions._CONST_VARIABLE_ID_CONFIG, setup_conn.get(ParamDefinitions._CONST_VARIABLE_ID_CONFIG));
				message_object.put(ParamDefinitions._CONST_VARIABLE_MSG_TYPE, setup_conn.get(ParamDefinitions._CONST_VARIABLE_MSG_TYPE));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	 
 		SendQueue sq = new  SendQueue(queue, jmsTemplate, message_object.toString());
 		 Thread nt = new Thread(sq);
 		 nt.start();
 	}
     
     private void runProcessorQueue() {
 		/*proccesor_queue_in= new ProccesorQueue(this.id_server,jmsTemplate,true,socket_client);
 		Thread thread1 = new Thread(proccesor_queue_in, "Processor Queue IN "+this.id_server);
         thread1.start();
         */
 		proccesor_queue_out= new ProccesorQueue(this.id_server,jmsTemplate,false,socket_client);
 		Thread thread2 = new Thread(proccesor_queue_out, "Processor Queue OUT "+this.id_server);
         thread2.start();
 	} 
     
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			runProcessorQueue();	
		while(socket_client.isConnected())
		{
		DataInputStream dis = new DataInputStream(socket_client.getInputStream());
        DataOutputStream dos = new DataOutputStream(socket_client.getOutputStream());
        
        byte[] buffer = null;
        short sSize = dis.readShort();
        buffer = new byte[sSize];
        dis.readFully(buffer);
        System.out.println("Rcved: " + new String(buffer));
        String data = new String(buffer);
        
       
       
            putQueue(data);
		}
		System.out.println("Finalizo la Conexion " + socket_client.getPort());
		}catch(Exception e)
		{
			//e.printStackTrace();
			System.out.println("Finalizo la Conexion " + socket_client.getPort());
		}
		try {
			socket_client.close();
			proccesor_queue_in.stop_queue();
			proccesor_queue_out.stop_queue();
			System.out.println("Conexion Cerrada " + socket_client.getPort());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
