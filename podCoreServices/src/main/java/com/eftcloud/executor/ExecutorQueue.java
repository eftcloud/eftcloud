package com.eftcloud.executor;

import javax.jms.Queue;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;

import com.eftcloud.params.ParamDefinitions;
import com.eftcloud.processor.GetInformationServices;



public class ExecutorQueue {
	private DelegateQueue proccesor_queue_in;
	private DelegateQueue proccesor_queue_out;
	 @Autowired
	    static JmsTemplate jmsTemplate;

	    @Autowired
	    static Queue queue;
	
	
	
	 public void runProcessorQueue() {
		 
		 
		 GetInformationServices.getDataConfig().forEach((key, value) -> {
				JSONObject item_value = new JSONObject();
				item_value=value;
				try {
					//Get Data
					int id_conn = Integer.parseInt(item_value.get(ParamDefinitions._CONST_VARIABLE_ID_CONFIG).toString());
							
					proccesor_queue_in= new DelegateQueue(id_conn,Config.jmsTemplateManual(),true,null);
			 		Thread thread1 = new Thread(proccesor_queue_in, "Processor Queue IN "+ id_conn);
			         thread1.start();
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

	 	} );
	 }
}
