package com.eftcloud.executor;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.jms.core.JmsTemplate;

import com.eftcloud.message.ProccesorMessage;
import com.eftcloud.params.ParamDefinitions;
import com.eftcloud.services.ConsumeWebService;

public class DelegateQueue  implements Runnable {

	 private boolean run_proccesor=false;
	 private boolean input_message=false;
	 private String queue_in="";
	 private String queue_out="";
	 private Session session;
	 private Connection connection = null;
	 private MessageConsumer consumer_in;
	 private MessageConsumer consumer_out;
	 private ProccesorMessage proc_msq = new ProccesorMessage();
	
	 private ConsumeWebService consumeWebService = new ConsumeWebService();
	 private Socket socket_client;
	
	 public void stop_queue()
	 {
		 this.run_proccesor=false;
	 }
	 
	public  DelegateQueue(int qn, JmsTemplate jmst,boolean inp,Socket s) {
		this.run_proccesor=true;
		this.queue_in="QUEUE_IN_" + qn;
		this.queue_out="QUEUE_OUT_" + qn;
		this.input_message=inp;
		this.socket_client=s;
		try {
		ConnectionFactory connectionFactory = jmst.getConnectionFactory();
		connection = connectionFactory.createConnection();

		
			this.session = connection.createSession(false,
			        Session.AUTO_ACKNOWLEDGE);
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	 
	public void proccesQueueInput()
	{
		Queue queue;
		Queue queue_out;
		try {
			queue = session.createQueue(this.queue_in);
			queue_out = session.createQueue(this.queue_out);
			MessageProducer producer = session.createProducer(queue_out);
			consumer_in = session.createConsumer(queue);
	        connection.start();
		
		
		while(run_proccesor) {
		
		//	for (int i = 0; i < 10; i++) {
                TextMessage textMsg = (TextMessage) consumer_in.receive();
               // System.out.println(textMsg);
                System.out.println("Read Queue: " + textMsg.getText());
                //Consume Core// Here
                JSONObject message_object = null;
				try {
					message_object = new JSONObject(textMsg.getText());
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
                JSONObject getMsg =proc_msq.getResponseMessage(message_object);
               /* try {
					getMsg = consumeWebService.POSTRequest(message_object, ParamDefinitions._CONST_CONSUME_WEBSERVICE_POST);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
                
                String response_msg= getMsg.toString();
                Message msg = session.createTextMessage(response_msg);
                producer.send(msg);
           // }
		}
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void proccesQueueOutput()
	{
		Queue queue;
		try {
			queue = session.createQueue(this.queue_out);
			consumer_out = session.createConsumer(queue);
	        connection.start();
	        BufferedOutputStream outStream = null;
		
		while(run_proccesor) {
		
		//	for (int i = 0; i < 10; i++) {
                TextMessage textMsg = (TextMessage) consumer_out.receive();
             //   System.out.println(textMsg);
                System.out.println("Received Queue Out:  " + textMsg.getText());
                
                if(socket_client!=null) {
                if (socket_client.isConnected()) {
    				try {
    					String rsp="";
    					try {
							JSONObject j_msg=new JSONObject(textMsg.getText());
							rsp=j_msg.getString("Message");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
    					byte[] bufferRsp = rsp.getBytes();
    					int sSize = bufferRsp.length;
    					DataOutputStream dos = new DataOutputStream(socket_client.getOutputStream());
    					dos.writeShort(sSize);
    	                dos.write(bufferRsp);
    	                dos.flush();

	    				
	    				System.out.println("-----Request sent to server---"+rsp);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

    			}
            }
		}
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		if (input_message)
		{
		proccesQueueInput();
		}else {
		proccesQueueOutput();
		}
	}
	
	public void stop()
	{
		run_proccesor=false;
	}
	

}
