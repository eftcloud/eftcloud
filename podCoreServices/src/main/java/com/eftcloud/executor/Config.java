package com.eftcloud.executor;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;

import com.eftcloud.params.ParamDefinitions;
import com.eftcloud.params.readConfig;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Queue;

@Configuration
public class Config {
	
	private String QueueAMQ="BusPersistence"; 
	private String QueueAMQ_IN=ParamDefinitions._CONST_AQM_QUEUE_IN;
	private String QueueAMQ_OUT=ParamDefinitions._CONST_AQM_QUEUE_OUT;
	
   //@Value(ParamDefinitions._CONST_AQM_SERVER)
    private String brokerUrl = readConfig.getVAR_PARAM_URL_AMQ();

    public String getQueueAMQ() {
		return QueueAMQ;
	}
    
    public String getQueueAMQ_IN() {
		return QueueAMQ_IN;
	}

    public String getQueueAMQ_OUT() {
		return QueueAMQ_OUT;
	}


	public void setQueueAMQ(String queueAMQ) {
		QueueAMQ = queueAMQ;
	}

	@Bean
    public Queue queue() {
        return new ActiveMQQueue(QueueAMQ);
    }

	@Bean
    public Queue queue_IN(int queue_number) {
        return new ActiveMQQueue(QueueAMQ_IN + queue_number);
    }
	
    @Bean
    public ActiveMQConnectionFactory activeMQConnectionFactory() {
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(readConfig.VAR_PARAM_USER_AMQ,readConfig.VAR_PARAM_PASS_AMQ, readConfig.VAR_PARAM_URL_AMQ );
        factory.setBrokerURL(brokerUrl);
        return factory;
    }


    public static ActiveMQConnectionFactory activeMQConnectionFactoryManual() {
    	 ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(readConfig.VAR_PARAM_USER_AMQ,readConfig.VAR_PARAM_PASS_AMQ, readConfig.VAR_PARAM_URL_AMQ );
         //factory.setBrokerURL(brokerUrl);
         Connection connection;
 		try {
 			connection = factory.createConnection();
 			 connection.start();
 		} catch (JMSException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
         
         return factory;
    }
    
    @Bean
    public JmsTemplate jmsTemplate() {
        return new JmsTemplate(activeMQConnectionFactory());
    }
    

    public static JmsTemplate jmsTemplateManual() {
        return new JmsTemplate(activeMQConnectionFactoryManual());
    }
}
