package com.eftcloud.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;

import com.eftcloud.executor.ExecutorQueue;
import com.eftcloud.params.readConfig;
import com.eftcloud.processor.Config;
import com.eftcloud.processor.GetInformationServices;

@SpringBootApplication// same as 
public class InitCoreService {
	@Autowired
	static
    JmsTemplate jmsTemplate;
	public static void main(String[] args) {
		SpringApplication.run(InitCoreService.class, args);
		readConfig r = new readConfig();
		r.readConfig_Property();
		GetInformationServices g = new GetInformationServices();
		g.getInfoWS();
		Config cfg = new Config();
		jmsTemplate=cfg.jmsTemplateManual();
		ExecutorQueue eq = new ExecutorQueue();
		eq.runProcessorQueue();
		
	}
}
