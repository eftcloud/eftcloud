package com.eftcloud.processor;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eftcloud.params.ParamDefinitions;
import com.eftcloud.params.readConfig;
import com.eftcloud.services.ConsumeWebService;



public class GetInformationServices {
	
	public static Map<Integer, JSONObject> getDataConfig() {
		return dataConfig;
	}

	public static Map <Integer, JSONObject> dataConfig =new HashMap<Integer, JSONObject>();
	 private ConsumeWebService consumeWebService = new ConsumeWebService();
	 
	public void getInfoWS()
	{
		try {
		JSONObject message_object = new JSONObject();
		JSONObject filters = new JSONObject();
		message_object.put(ParamDefinitions._CONST_VARIABLE_ID_QUERY, ParamDefinitions._QUERY_GET_ALL_SERVICES);
		
		message_object.put(ParamDefinitions._CONST_VARIABLE_FILTERS, filters);
		String rst= consumeWebService.POSTRequest(message_object, readConfig.getVAR_PARAM_ULR_WS());

        JSONArray rst_json = new JSONArray(rst);
        
        for (int i = 0; i < rst_json.length(); i++)
        {
            JSONObject jsonObj = rst_json.getJSONObject(i);
        	dataConfig.put(i, jsonObj);
            System.out.println(jsonObj);
        }

	
		}catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
