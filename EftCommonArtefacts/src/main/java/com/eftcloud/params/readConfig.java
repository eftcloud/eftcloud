package com.eftcloud.params;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class readConfig {
	
public static String VAR_PARAM_ULR_WS="";
public static String VAR_PARAM_USER_AMQ="";
public static String VAR_PARAM_PASS_AMQ="";
public static String VAR_PARAM_URL_AMQ="";

	
public void readConfig_Property()
{
	try (InputStream input = new FileInputStream("src/main/resources/application.properties")) {

        Properties prop = new Properties();

        // load a properties file
        prop.load(input);

        // get the property value and print it out
        this.VAR_PARAM_ULR_WS=prop.getProperty("db.url_ws");
        this.VAR_PARAM_USER_AMQ=prop.getProperty("db.user_amq");
        this.VAR_PARAM_PASS_AMQ=prop.getProperty("db.pass_amq");
        this.VAR_PARAM_URL_AMQ=prop.getProperty("db.url_amq");

    } catch (IOException ex) {
        ex.printStackTrace();
    }

}

public static void main(String arg[])
{
	readConfig  r = new readConfig();
}

public static String getVAR_PARAM_ULR_WS() {
	return VAR_PARAM_ULR_WS;
}

public static String getVAR_PARAM_USER_AMQ() {
	return VAR_PARAM_USER_AMQ;
}

public static String getVAR_PARAM_PASS_AMQ() {
	return VAR_PARAM_PASS_AMQ;
}

public static String getVAR_PARAM_URL_AMQ() {
	return VAR_PARAM_URL_AMQ;
}



}
