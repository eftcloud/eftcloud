package com.eftcloud.params;

public class ParamDefinitions {
//PARAMS FOR RESPONSE TYPES
	public static final String _CONST_RESPONSE_CODE_00 ="00";
	

	
		public static String _CONST_AQM_QUEUE_IN ="QUEUE_IN_";
		public static String _CONST_AQM_QUEUE_OUT ="QUEUE_OUT_";
	
		/*public static String _CONST_AQM_SERVER ="tcp://localhost:61616";
		public static String _CONST_AQM_SERVER_USER ="admin";
		public static String _CONST_AQM_SERVER_PASS ="admin";

		public static String _CONST_MONGODB_SERVER ="mongodb://localhost:27017";
		public static String _CONST_CONSUME_WEBSERVICE_POST ="http://localhost:8090/rest/consumeCore/";
		public static int _CONST_TIMEOUT_READ=1000;
		public static int _CONST_TIMEOUT_CONN=1000;
	
	/*public static String _CONST_AQM_SERVER = System.getenv("_CONST_AQM_SERVER");
	public static String _CONST_CONSUME_WEBSERVICE_POST=System.getenv("_CONST_CONSUME_WEBSERVICE_POST");
	public static String _CONST_AQM_SERVER_USER=System.getenv("_CONST_AQM_SERVER_USER");
	public static String _CONST_AQM_SERVER_PASS=System.getenv("_CONST_AQM_SERVER_PASS");
	public static String _CONST_AQM_QUEUE_IN=System.getenv("_CONST_AQM_QUEUE_IN");
	public static String _CONST_AQM_QUEUE_OUT=System.getenv("_CONST_AQM_QUEUE_OUT");
	public static int _CONST_TIMEOUT_READ=Integer.parseInt(System.getenv("_CONST_TIMEOUT_READ"));
	public static int _CONST_TIMEOUT_CONN=Integer.parseInt(System.getenv("_CONST_TIMEOUT_CONN"));
	
/*
	//OPenshift
		public static final String _CONST_AQM_SERVER ="tcp://amqbusmanager-amq-tcp:61616";
		public static final String _CONST_MONGODB_SERVER ="mongodb://admin:whKBcBNPFERy4XUp@mongodb:27017";
		public static final String _CONST_CONSUME_WEBSERVICE_POST ="http://podpersistencedb-route-fiduprevisoradev.cloudappsdesa.fiduprevisora.loc/rest/podDataInterfacePost/";
*/	
	
		
		
		public static final String _CONST_TYPE_SERVICES_WEBSERVICE ="01";
		public static final String _CONST_TYPE_SERVICES_FILE ="02";
		public static final String _CONST_TYPE_SERVICES_DATABASE ="03";
		
		public static final String _CONST_TYPE_SERVICE_SERVER ="SERVER";
		public static final String _CONST_TYPE_SERVICE_CLIENT ="CLIENT";
		
		
		public static final String _CONST_VARIABLE_TYPE_SERVICE ="type_service";
		public static final String _CONST_VARIABLE_IP_ADDRESS ="ip_config";
		public static final String _CONST_VARIABLE_PORT_SERVER ="port_config";
		public static final String _CONST_VARIABLE_PROTOCOL="protocol";
		public static final String _CONST_VARIABLE_MSG_TYPE ="type_message";
		public static final String _CONST_VARIABLE_ID_QUERY ="id_query";
		public static final String _CONST_VARIABLE_FILTERS ="filters";
		public static final String _CONST_VARIABLE_MESSAGE ="message";
		public static final String _CONST_VARIABLE_SOCKET_ID ="socket_id";
		public static final String _CONST_VARIABLE_TIME_REQUEST ="time_request";
		public static final String _CONST_VARIABLE_RESPONSE ="response";
		public static final String _CONST_VARIABLE_TIME_RESPONSE ="time_response";
		
		//Constans for Message Query 1
		public static final String _CONST_VARIABLE_ID_CONFIG ="id_config";
		public static final String _CONST_VARIABLE_NAME_CONFIG ="name_config";
		public static final String _CONST_VARIABLE_PHYS_CONN ="config_physical";
		
		
		//FOR MESSAGE TYPES
		public static final String _CONST_TYPE_MSG_ISO8583="ISO8583";
		//SQL STATEMENTS INDEX
		public static final String _QUERY_GET_ALL_SERVICES ="1";
				
}
